package me.dwliu.lab.emqtt.controller;

import me.dwliu.lab.emqtt.server.IMqttSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * MQTT消息发送
 *
 * @author liudw
 * @date 2020-03-10 13:21
 **/
@RestController
@RequestMapping(value = "/")
public class MqttController {

    /**
     * 注入发送MQTT的Bean
     */
    @Autowired
    private IMqttSender iMqttSender;

    /**
     * 发送MQTT消息
     *
     * @param message 消息内容
     * @return 返回
     */
    @GetMapping(value = "/mqtt", produces = "text/html")
    public ResponseEntity<String> sendMqtt(@RequestParam(value = "msg") String message) {
        iMqttSender.sendToMqtt(message);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

    /**
     * 发送MQTT消息 指定 topic
     *
     * @param message 消息内容
     * @return 返回
     */
    @GetMapping(value = "/mqtt2", produces = "text/html")
    public ResponseEntity<String> sendMqtt2(@RequestParam(value = "msg") String message) {
        iMqttSender.sendToMqtt("topic-demo111", message);
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }
}
