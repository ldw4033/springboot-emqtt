> 使用Emqtt进行mqtt协议的传输发布消息。通过订阅对应的topic进行推送内容。


# 第三方

* [Emqtt](http://www.emqtt.com/)
   ```
   $ docker run -d --name emqx31 -p 1883:1883 -p 8083:8083 -p 8883:8883 -p 8084:8084 -p 18083:18083 emqx/emqx:v4.0.0
   ```
* maven依赖
```
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-integration</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.integration</groupId>
            <artifactId>spring-integration-stream</artifactId>
        </dependency>
         <dependency>
            <groupId>org.springframework.integration</groupId>
            <artifactId>spring-integration-mqtt</artifactId>
         </dependency>
```
* lombok

# 配置和代码介绍

* 进行配置相关的Mqtt的配置项
```$xslt
###################
#  #  MQTT 配置
#  ##################
#  # 用户名
#  mqtt.username=admin
#  # 密码
#  mqtt.password=password
#  # 推送信息的连接地址，如果有多个，用逗号隔开，如：tcp://127.0.0.1:61613,tcp://192.168.1.61:61613
#  mqtt.url=tcp://127.0.0.1:61613
#  ##################
#  #  MQTT 生产者
#  ##################
#  # 连接服务器默认客户端ID
#  mqtt.producer.clientId=mqttProducer
#  # 默认的推送主题，实际可在调用接口时指定
#  mqtt.producer.defaultTopic=topic1
#  ##################
#  #  MQTT 消费者
#  ##################
#  # 连接服务器默认客户端ID
#  mqtt.consumer.clientId=mqttConsumer
#  # 默认的接收主题，可以订阅多个Topic，逗号分隔
#  mqtt.consumer.defaultTopic=topic1
mqtt:
  username: admin
  password: public
  url: tcp://10.10.1.60:1883
  producer:
    clientId: mqttProducer
    defaultTopic: topic1
  consumer:
    clientId: mqttConsumer
    defaultTopic: topic1
```

